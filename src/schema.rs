use juniper::FieldResult;
use juniper::RootNode;

#[derive(GraphQLEnum)]
pub enum DocsType {
    Javadocs,
    RustDocs,
    DocsRs,
}

#[derive(GraphQLObject)]
pub struct Docs {
    name: String,
    source_url: String,
    docs_type: DocsType,
    root_module: Module,
}

#[derive(GraphQLObject)]
pub struct Struct {
    name: String,
    module: Module,
    description: Option<String>,
    traits: Vec<Trait>,
    methods: Vec<Function>,
    fields: Vec<Field>,
    types: Vec<TypeDefinition>,
}

#[derive(GraphQLObject)]
pub struct Enum {
    name: String,
    module: Module,
    description: Option<String>,
    variants: Vec<Struct>,
}

#[derive(GraphQLObject)]
pub struct Trait {
    name: String,
    module: Module,
    description: Option<String>,
    methods: Vec<Function>,
    types: Vec<TypeDefinition>,
}

#[derive(GraphQLObject, Default)]
pub struct Module {
    name: String,
    description: String,
    structs: Vec<Struct>,
    enums: Vec<Enum>,
    traits: Vec<Trait>,
    modules: Vec<Module>,
    constants: Vec<Constant>,
    functions: Vec<Function>,
    types: Vec<TypeDefinition>,
}

#[derive(GraphQLObject)]
pub struct Constant {
    name: String,
    module: Module,
    description: Option<String>,
    s_type: TypeDefinition,
    value: String,
}

#[derive(GraphQLObject)]
pub struct Function {
    name: String,
    module: Module,
    description: Option<String>,
}

#[derive(GraphQLObject)]
pub struct TypeDefinition {
    name: String,
    module: Module,
    description: Option<String>,
    parameters: Vec<TypeDefinition>,
}

#[derive(GraphQLObject)]
pub struct Field {
    name: String,
    s_struct: Struct,
    s_type: TypeDefinition,
}

pub struct QueryRoot;
graphql_object!(QueryRoot: () |&self| {
    field docs(&executor, url: String, docs_type: DocsType) -> FieldResult<Docs> {
        Ok(Docs {
            name: "A Docs".to_owned(),
            source_url: url,
            docs_type: docs_type,
            root_module: Module{
                name: "root".to_owned(),
                description: "The root module".to_owned(),
                ..Module::default()
            }
        })
    }
});
pub struct MutationRoot;
graphql_object!(MutationRoot: () |&self| {});

pub type Schema = RootNode<'static, QueryRoot, MutationRoot>;

pub fn create_schema() -> Schema {
    Schema::new(QueryRoot {}, MutationRoot {})
}