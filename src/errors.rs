use core::fmt::{self, Display};
use std::fmt::Formatter;

use config::ConfigError;
use failure::{Error, Fail};

#[derive(Debug, Fail)]
pub enum SettingsError {
    BadTypeError {
        key: Option<String>,
        expected: String,
        unexpected: String,
    },
    MissingEntryError {
        key: String,
    },
    UnknownError {
        #[fail(cause)]
        cause: Error,
    },
}

impl Display for SettingsError {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match *self {
            SettingsError::BadTypeError { ref key, ref expected, ref unexpected } => write!(f, "invalid type for `{}`, found {} when expected {}", key.clone().unwrap_or("<unknown>".into()), unexpected, expected),
            SettingsError::MissingEntryError { ref key } => write!(f, "missing value for `{}`", key),
            SettingsError::UnknownError { ref cause } => write!(f, "unknown error, caused: {}", cause),
        }
    }
}

pub fn into_settings_error(e: ConfigError) -> SettingsError {
    match e {
        ConfigError::NotFound(key) => SettingsError::MissingEntryError { key },
        ConfigError::Type { origin: _, unexpected, expected, key } => SettingsError::BadTypeError { key, expected: expected.into(), unexpected: format!("{}", unexpected) },
        e => SettingsError::UnknownError { cause: e.into() },
    }
}