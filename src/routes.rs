use actix_web::AsyncResponder;
use actix_web::FutureResponse;
use actix_web::HttpRequest;
use actix_web::HttpResponse;
use actix_web::Json;
use actix_web::State;
use futures::future::Future;
use juniper::http::graphiql::graphiql_source;
use crate::AppState;
use crate::graphql::GraphQLData;

/// The Index route
pub fn index(_req: &HttpRequest<AppState>) -> HttpResponse {
    HttpResponse::Ok().content_type("text/plain; charset=utf-8").body("Welcome on DocsAPI =)")
}

/// The GraphQL IDE online =)
pub fn graphiql(_req: &HttpRequest<AppState>) -> HttpResponse {
    let html = graphiql_source("http://127.0.0.1:8080/graphql");
    HttpResponse::Ok()
        .content_type("text/html; charset=utf-8")
        .body(html)
}

/// The future GraphQL entpoint
pub fn graphql((st, data): (State<AppState>, Json<GraphQLData>)) -> FutureResponse<HttpResponse> {
    st.executor
        .send(data.0)
        .from_err()
        .and_then(|res| match res {
            Ok(user) => Ok(HttpResponse::Ok()
                .content_type("application/json")
                .body(user)),
            Err(_) => Ok(HttpResponse::InternalServerError().into()),
        }).responder()
}