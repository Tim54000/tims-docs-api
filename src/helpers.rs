use std::io;
use std::process;
use chrono::Local;
use config::Config;
use failure::Error;
use flexi_logger::{Cleanup, Record};
use crate::errors::into_settings_error;

// Helper functions

/// Log formater
pub fn log_format(w: &mut io::Write, record: &Record) -> Result<(), io::Error> {
    write!(
        w,
        "[{}] {} {}",
        Local::now().format("%Y-%m-%d %H:%M:%S%.6f %:z"),
        record.level(),
        &record.args()
    )
}

/// Represents the entire `settings` configuration file
pub struct Settings {
    pub name: String,
    pub allowed_threads: usize,
    pub servers: Vec<Server>,
    pub log: LogSettings,
}

/// Represents the log section in settings
pub struct LogSettings {
    pub level: String,
    pub path: String,
    pub rotate_over: usize,
    pub cleanup: Cleanup,
}

/// Represents a server
#[derive(Debug, Clone)]
pub struct Server {
    pub name: String,
    pub ips: Vec<String>,
    pub enable_ssl: bool,
    pub private_key_path: Option<String>,
    pub cert_path: Option<String>,
}

impl Settings {
    pub fn from_config(config: &Config) -> Result<Self, Error> {
        Ok(Settings {
            name: config.get_str("general.name").unwrap_or("DocsAPI".into()),
            allowed_threads: config.get_int("general.threads").unwrap_or(4) as usize,
            log: LogSettings::from_config(&config),
            servers: config.get_array("general.enabled_servers")
                .map_err(|e| into_settings_error(e).into()) // Convert `ConfigError` into `failure::Error`
                .and_then(|enabled_server| { // If we have a Ok result + get `general.enabled_servers`
                    enabled_server.iter().map(|s| { // For each server name, create the linked-Server object
                        let servername = s.clone().into_str().unwrap();
                        Server::from_config(&config, &("servers.".to_owned() + &servername), servername) // Return a result -> if there is an error, stop and return SettingsError
                    }).collect::<Result<Vec<Server>, Error>>() // Abort for any error
                })?,
        })
    }
}

impl LogSettings {
    pub fn from_config(config: &Config) -> Self {
        let cleanup = match (config.get_int("LOG_CLEANUP_OVER").unwrap_or(0) as usize, config.get_int("ZIP_CLEANUP_OVER").unwrap_or(0) as usize) {
            (0, 0) => Cleanup::Never, // Keep all forever
            (0, x) => Cleanup::KeepZipFiles(x), // Keep only x zip files
            (x, 0) => Cleanup::KeepLogFiles(x), // Keep only x log files
            (a, b) => Cleanup::KeepLogAndZipFiles(a, b) // Keep only a log files and b zip files
        };

        LogSettings {
            level: config.get_str("log.level").unwrap_or("info".into()),
            path: config.get_str("log.path").unwrap_or("./logs/".into()),
            cleanup,
            rotate_over: (config.get_float("log_rotate_over").unwrap_or(50.0) * 1_000_000.0) as usize,
        }
    }
}

impl Server {
    pub fn from_config(config: &Config, path: &str, name: String) -> Result<Self, Error> {
        Ok(Server { // Return a Server object in Result
            name,
            ips: config.get_array((path.to_owned() + ".listen").as_str()).and_then(|ips| { // Make sure to get IPs addresses -- `servers.<name>.listen`
                let ips: Result<Vec<String>, _> = ips.iter().map(|ip| { // Convert `ip` from Config::Value to String
                    ip.clone().into_str()
                }).collect(); // Abort on any error!
                Ok(ips?)
            }).map_err(|e| into_settings_error(e))?,
            enable_ssl: config.get_bool((path.to_owned() + ".enable_ssl").as_str()).map_err(|e| into_settings_error(e))?,
            cert_path: config.get_str((path.to_owned() + ".cert_path").as_str()).ok(),
            private_key_path: config.get_str((path.to_owned() + ".private_key_path").as_str()).ok(),
        })
    }
}

pub fn exit_error(reason: &str, error: Error, code: i32) -> ! {
    eprintln!("{} {}", reason, error);
    process::exit(code)
}