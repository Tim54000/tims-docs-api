extern crate actix_web;
extern crate chrono;
extern crate config;
extern crate flexi_logger;
extern crate futures;
extern crate url;
#[macro_use]
extern crate juniper;
#[macro_use]
extern crate log;
extern crate rustls;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;

use std::fs::File;
use std::io::BufReader;
use actix::Addr;
use actix::SyncArbiter;
use actix_web::{App, middleware};
use actix_web::server;
use actix_web::server::HttpServer;
use chrono::Local;
use failure::{err_msg, Error};
use rustls::{NoClientAuth, ServerConfig};
use rustls::internal::pemfile::{certs, rsa_private_keys};
use server::ServerFlags;
use helpers::{exit_error, log_format, Settings};
use crate::graphql::GraphQLExecutor;
use crate::helpers::Server;
use crate::schema::create_schema;

mod helpers;
mod routes;
mod errors;
mod schema;
mod graphql;
mod parser;

pub struct AppState {
    executor: Addr<GraphQLExecutor>,
}

fn main() {
    println!("Hello, world!");

    // Init settings & logs
    // SETTINGS
    let mut config = config::Config::new();
    config.merge(config::File::with_name("settings")).unwrap_or_else(|err| exit_error("Error on parsing settings from file:", err.into(), 1));
    let settings = Settings::from_config(&config).unwrap_or_else(|err| exit_error("Error on settings:", err, 1));

    // LOGS
    let mut log = flexi_logger::Logger::with_env_or_str(settings.log.level)
        .format(|w, r| log_format(w, r))
        .directory(settings.log.path)
        .log_to_file()
        .print_message()
        .discriminant(format!("{}", Local::now().format("%d-%m-%YT%H:%M:%S")))
        .duplicate_to_stderr(flexi_logger::Duplicate::Info);
    if settings.log.rotate_over > 0 {
        log = log.rotate(settings.log.rotate_over, settings.log.cleanup);
    }
    log.start()
        .unwrap();

    // Init server
    let sys = actix::System::new(settings.name);
    let schema = std::sync::Arc::new(create_schema());
    let addr = SyncArbiter::start(settings.allowed_threads, move || GraphQLExecutor::new(schema.clone()));
    let mut server = HttpServer::new(
        move || {
            App::with_state(AppState { executor: addr.clone() })
                .middleware(middleware::Logger::default())
                .resource("/", |r| { r.get().h(routes::index) })
                .resource("/graphql", |r| { r.post().with(routes::graphql) })
                .resource("/graphiql", |r| { r.get().h(routes::graphiql) })
        });

    // Bind servers on IP addresses
    for s in settings.servers {
        match s.enable_ssl {
            true => {
                // load ssl keys
                let config = build_ssl(s.clone()).unwrap_or_else(|err| exit_error(&format!("Error on SSL settings for {}:", s.name), err.into(), 1));

                // actix acceptor
                let acceptor = server::RustlsAcceptor::with_flags(
                    config,
                    ServerFlags::HTTP1 | ServerFlags::HTTP2,
                );

                //bind IP addresses with SSL support
                for ip in s.ips {
                    let a = acceptor.clone();
                    server = server.bind_with(ip, move || a.clone()).unwrap_or_else(|err| exit_error("Bind error:", err.into(), 1));
                }
            }
            false => {
                //bind IP addresses
                for ip in s.ips {
                    warn!("IP {} issued without SSL", &ip);
                    server = server.bind(ip).unwrap_or_else(|err| exit_error("Bind error:", err.into(), 1));
                }
            }
        }
    }
    server.start();
    let _ = sys.run();
}

fn build_ssl(server: Server) -> Result<ServerConfig, Error> {
    let mut config = ServerConfig::new(NoClientAuth::new());
    let cert_file = &mut BufReader::new(File::open(server.cert_path.expect("Cert file doesn't set"))?);
    let key_file = &mut BufReader::new(File::open(server.private_key_path.expect("Private key doesn't set"))?);
    let cert_chain = certs(cert_file).map_err(|_| err_msg("invalid certificate"))?;
    let mut keys = rsa_private_keys(key_file).map_err(|_| err_msg("invalid private key"))?;
    config.set_single_cert(cert_chain, keys.remove(0))?;
    Ok(config)
}