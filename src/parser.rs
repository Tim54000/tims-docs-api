use url::Url;
use failure::{Error, err_msg};
use crate::schema::DocsType;
use crate::schema::Docs;

struct DocsParser {
    url: Url,
    doctype: DocsType,
}

impl DocsParser {
    fn new(url: String, doctype: DocsType) -> Result<Self, Error> {
        let url = Url::parse(&url)?;
        Ok(DocsParser { url, doctype })
    }
    fn parse(&mut self) -> Result<Docs, Error> {
        unimplemented!("Thinking =)")
    }
}

